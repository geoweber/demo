package com.example.boot.demo.security;


import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import javax.transaction.Transactional;

@Transactional
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    List<Customer> findByLastName(String lastName);
}