package com.example.boot.demo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CustomerService {

    @Autowired
    private CustomerRepository repository;

    public List<Customer> findAll(){


        // save a couple of customers
       // repository.save(new Customer("Jack", "Bauer"));


        return repository.findAll();
    }

}
